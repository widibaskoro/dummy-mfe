import dynamic from "next/dynamic";

const Filter = dynamic(() => import("bangkokok/Filter").then(ui => ui.Filter), {
  ssr: false,
});

const FilterCheckbox = dynamic(() => import("bangkokok/Filter").then(ui => ui.FilterCheckbox), {
  ssr: false,
});

const FilterField = dynamic(() => import("bangkokok/Filter").then(ui => ui.FilterField), {
  ssr: false,
});

const FilterDate = dynamic(() => import("bangkokok/Filter").then(ui => ui.FilterDate), {
  ssr: false,
});

export default function FilterTopUp() {
  return (
    <Filter>
      {(onChange, handleEnter, handleChangeWithIndex) => (
        <>
          <FilterCheckbox
            data-qaid="qa-filter-parent-type"
            title="Parent Type"
            keyFilter="parent_type"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            // data={TRANSACTION_TYPE}
          />
          <FilterCheckbox
            data-qaid="qa-filter-sender-bank"
            title="Sender Bank"
            keyFilter="sender_bank"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            // data={SENDER_BANK}
          />
          <FilterCheckbox
            data-qaid="qa-filter-beneficiary-type"
            title="Beneficiary Type"
            keyFilter="beneficiary_type"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            // data={USER_TYPE}
          />
          <FilterCheckbox
            data-qaid="qa-filter-user-type"
            title="User Type"
            keyFilter="user_type"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            // data={USER_TYPE}
          />
          <FilterCheckbox
            data-qaid="qa-filter-status"
            title="Status"
            keyFilter="status"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            // data={PENDING_TOPUP_STATUS}
          />

          <FilterField
            data-qaid="qa-filter-id"
            title="ID"
            keyFilter="id"
            onChange={onChange}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-parent-id"
            title="Parent ID"
            keyFilter="parent_id"
            onChange={onChange}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-user-id"
            title="User ID"
            keyFilter="user_id"
            onChange={onChange}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-sender-name"
            title="Sender Name"
            keyFilter="sender_name"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-beneficiary-id"
            title="Beneficiary ID"
            keyFilter="beneficiary_id"
            onChange={onChange}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-amount"
            title="Amount"
            keyFilter="amount"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-unique-code"
            title="Unique Code"
            keyFilter="unique_code"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-total-amount"
            title="Total Amount"
            keyFilter="total_amount"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-company-id"
            title="Company ID"
            keyFilter="company_id"
            onChange={onChange}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-credit-ref"
            title="Credit Ref"
            keyFilter="credit_ref"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            onKeyDown={handleEnter}
          />
          <FilterField
            data-qaid="qa-filter-debit-ref"
            title="Debit Ref"
            keyFilter="debit_ref"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
            onKeyDown={handleEnter}
          />
          <FilterDate
            data-qaid="qa-filter-created-at"
            title="Created At"
            keyFilter="created_at"
            onChange={onChange}
          />
          <FilterDate
            data-qaid="qa-filter-confirmed-at"
            title="Confirmed At"
            keyFilter="confirmed_at"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
          />
          <FilterDate
            data-qaid="qa-filter-completed-at"
            title="Completed At"
            keyFilter="completed_at"
            onChange={(e) => handleChangeWithIndex(e, "created_at", 1)}
          />
        </>
      )}
    </Filter>
  );
}
