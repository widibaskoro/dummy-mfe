import "src/assets/styles/global.scss";
import "@module-federation/nextjs-mf/lib/include-defaults";

import type { AppProps } from 'next/app'

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}

export default MyApp
