const {
  breakpoints,
  colors,
  extendedSpacing,
  gridRequiredUtils,
  hoverColors,
  textColors,
  bgColors,
  whitelistGrid,
} = require("./tailwind-config-utils");

module.exports = {
  corePlugins: {
    // disable preflight | https://tailwindcss.com/docs/preflight/#app
    preflight: false,
  },

  // make all utils important
  // important: false,
  important: "#root",

  content: ["src/**/*.{tsx,js,ts,jsx}"],
  safelist: [
    ...textColors,
    ...bgColors,
    ...gridRequiredUtils,
    ...whitelistGrid,
  ],

  // add prefix
  prefix: "u-",

  theme: {
    // override default breakpoints to same as bootstrap (current bigflip breakpoints)
    screens: {
      ...breakpoints,

      // additional breakpoints for Grid component
      "sm-con": { min: "576px", max: "767px" },
      "md-con": { min: "768px", max: "991px" },
      "lg-con": { min: "992px", max: "1199px" },
      "xl-con": { min: "1200px" },
    },

    // extend tailwind utilities
    extend: {
      // additional maxWidth
      maxWidth: {
        "screen-sm": "540px",
        "screen-md": "720px",
        "screen-lg": "960px",
        "screen-xl": "1140px",
        "3/5": "60%",
      },
      // additional minWidth
      minWidth: {
        "1/3": "33.333333%",
      },
      // extend font-size with current flip font-size utils
      fontSize: {
        tiny: "0.5625rem",
        small: "0.75rem",
        semi: "0.8125rem",
        base: "0.875rem",
        medium: "1rem",
        large: "1.125rem",
        huge: "1.75rem",
      },

      // extend color theme
      // e.g text-flip-orange; bg-flip-orange; border-flip-orange etc
      colors: { ...colors, ...hoverColors },

      // extend spacing utilities. Tailwind use 16px as base font-size, so its same with ours
      // however it doesnt have 7 and 9, so we add it
      spacing: {
        ...extendedSpacing,
      },
    },
  },
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "disabled"],
    placeholderColor: ["responsive", "focus", "disabled"],
    extend: {
      margin: ["last"],
    },
  },
  plugins: [],
};
