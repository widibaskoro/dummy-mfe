import { useState } from "react";

import dynamic from "next/dynamic";
import Filter from "../components/Filters/FilterTopUp";
import { TOPUP_INDEX_TABLE_HEADER } from "../consts/tables";
import { useFlash as FlashContext } from "../hooks/flash";

const Table = dynamic(() => import("bangkokok/ui").then((ui) => ui.Table), {
  ssr: false,
});

const Button = dynamic(() => import("bangkokok/ui").then((ui) => ui.Button), {
  ssr: false,
});

const WideTable = dynamic(
  () => import("bangkokok/WideTable").then((ui) => ui.WideTable),
  { ssr: false }
);

const SearchById = dynamic(() => import("bangkokok/SearchById"), {
  ssr: false,
});

const TableHeader = dynamic(() => import("bangkokok/TableHeader"), {
  ssr: false,
});

const Pagination = dynamic(() => import("bangkokok/Pagination"), {
  ssr: false,
});

const ActionButton = dynamic(() => import("bangkokok/ActionButton"), {
  ssr: false,
});

function useTableHeaders(headers) {
  const result = {};

  headers.forEach((item) => {
    result[item.key] = {
      title: item.title,
      sort: item.sort,
      width: item.width,
    };
  });
  return result;
}

export default function TopUpIndexPage() {
  const [filterVisible, setFilterVisible] = useState(false);

  const tableHeaders = useTableHeaders(TOPUP_INDEX_TABLE_HEADER);

  const handleToggleFilter = () => {
    setFilterVisible(!filterVisible);
  };

  return (
    <>
    <FlashContext.Provider>
        <div data-qaid="qa-tu-index-page" className="u-flex">
          {filterVisible && <Filter />}
          <span className="u-ml-2 u-items-center u-text-small u-m-0 u-text-center" />
          <div className="u-w-full">
            <div className="u-py-2 u-px-5 u-mb-2">
              <h5 className="u-text-lg u-mb-4">Top Up Transfer REMOTE APP</h5>

              <Button
                data-qaid="qa-tu-index-filter-button"
                outline={filterVisible ? "default" : "grey"}
                color="blue"
                variant="mini"
                className="u-mr-2 u-justify-between"
                onClick={handleToggleFilter}
              >
                Filter
              </Button>
            </div>
            <div className="u-max-w-md u-px-5">
              <SearchById />
            </div>

            <WideTable filterVisible={filterVisible} className="u-mt-2">
              <Table type="bordered" className="u-border-t u-border-disabled">
                <TableHeader headers={tableHeaders} />
                <tbody>
                  <tr className="u-align-top" key={1}>
                    <td>
                      12345
                      <ActionButton
                        textCopy="12345"
                        detailsLink={`/transfer/topups/12345`}
                        className="u-ml-0"
                      />
                    </td>
                    <td className="u-whitespace-nowrap">Top up</td>
                    <td>Top up</td>
                    <td>
                      1234
                      <ActionButton textCopy="1234" className="u-ml-0" />
                    </td>
                    <td>Test</td>
                    <td>BCA</td>
                    <td>not set</td>
                    <td>Regular</td>
                    <td>22</td>
                    <td>100000</td>
                    <td>50</td>
                  </tr>
                </tbody>
              </Table>
            </WideTable>
            <div className="u-align-center u-justify-center u-flex u-py-6">
              <Pagination meta={{}} />
            </div>
          </div>
        </div>
    </FlashContext.Provider>
    </>
  );
}
