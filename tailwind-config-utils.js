const { darken, desaturate } = require("polished");

const breakpoints = {
  xs: "0px",
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1200px",
};

const extendedSpacing = {
  "50x": "50px",
  84: "21rem",
  "1/12": "8.333333%",
  "2/12": "16.666667%",
  "3/12": "25%",
  "4/12": "33.333333%",
  "5/12": "41.666667%",
  "6/12": "50%",
  "7/12": "58.333333%",
  "8/12": "66.666667%",
  "9/12": "75%",
  "10/12": "83.333333%",
  "11/12": "91.666667%",
  "12/12": "100%",
};

// prettier-ignore
const colors = {
  // Shade and Tints: Flip Orange
  "cinnamon": "#BC4B31",
  "brick": "#DF593A",
  "flip-orange": "#FD6542",
  "peach": "#FD8468",
  "anemia": "#FEC1B3",

  // Shade and Tints: Black Bekko
  "black-bekko": "#333333",
  "dark-smoke": "#545454",
  "dark-grey": "#A9A9A9",
  "light-grey": "#DEDEDE",
  "light-smoke": "#F9F9F9",

  // Shade and Tints: Ketchup Tomato
  "red-wine": "#990F28",
  "clown-red": "#B61F3C",
  "ketchup-tomato": "#D42E52",
  "blush": "#E58297",
  "sakura": "#FBEBEE",

  // Shade and Tints: Mango Yellow
  "coin": "#64500C",
  "goldenrod": "#DBA314",
  "mango-yellow": "#FFC122",
  "blonde": "#FFDA7A",
  "banana": "#FFF8E5",

  // Shade and Tints: Intel Blue
  "space-blue": "#034C87",
  "pepsi": "#0464B1",
  "intel-blue": "#067AD8",
  "tie-dye": "#6AAFE8",
  "bubble": "#DCEFFF",

  // Shade and Tints: Jade
  "spinach": "#06604C",
  "pine": "#078167",
  "jade": "#00BC93",
  "slight-jade": "#91DEC6",
  "light-jade": "#E9FFF8",

  // Punchy Colors
  "warning": "#D41A1A",
  "sunrise": "#FFDB1E",
  "sky-blue": "#2FC0F0",
  "greeneon": "#22D48C",
  "catskill-white": "#F3F7F9",

  // Miscellaneous Colors
  "white": "#FFFFFF",
  "table-stripe": "#FCFDFC",
  "main-background": "#F3F5F6",
  "disabled": "#F2F2F2",
  "light-anemia": "#FFF5F3"
};

const gridRequiredUtils = [
  "u-order-first",
  "u-order-last",
  "u-order-1",
  "u-order-2",
  "u-order-3",
  "u-order-4",
  "u-order-5",
  "u-order-6",
  "u-order-7",
  "u-order-8",
  "u-order-9",
  "u-order-10",
  "u-order-11",
  "u-order-12",
  "u-w-auto",
  "u-flex-1",
  "u-flex-none",
  "u-flex",
  "u-flex-wrap",
  "u-mx-auto",
  "u-max-w-none",
  "u-w-1/12",
  "u-w-2/12",
  "u-w-3/12",
  "u-w-4/12",
  "u-w-5/12",
  "u-w-6/12",
  "u-w-7/12",
  "u-w-8/12",
  "u-w-9/12",
  "u-w-10/12",
  "u-w-11/12",
  "u-w-12/12",
  "u-w-1/5",
  "u-ml-1/12",
  "u-ml-2/12",
  "u-ml-3/12",
  "u-ml-4/12",
  "u-ml-5/12",
  "u-ml-6/12",
  "u-ml-7/12",
  "u-ml-8/12",
  "u-ml-9/12",
  "u-ml-10/12",
  "u-ml-11/12",
  "u-ml-12/12",
];

const gridContainerRequiredUtils = [
  "sm-con:u-max-w-none",
  "md-con:u-max-w-none",
  "lg-con:u-max-w-none",
  "xl-con:u-max-w-none",
  "sm-con:u-mx-0",
  "md-con:u-mx-0",
  "lg-con:u-mx-0",
  "xl-con:u-mx-0",
  "sm:u-max-w-screen-sm",
  "md:u-max-w-screen-md",
  "lg:u-max-w-screen-lg",
  "xl:u-max-w-screen-xl",
];

function getHoverColors() {
  const all = {};
  Object.keys(colors).forEach(
    (c) => (all[c + "-h"] = darken(0.1, desaturate(0.2, colors[c])))
  );
  return all;
}

function getGridWithBreakpoints() {
  const result = [];
  Object.keys(breakpoints).forEach((b) => {
    gridRequiredUtils.forEach((g) => {
      result.push(`${b}:${g}`);
    });
  });
  return result;
}

module.exports = {
  breakpoints,
  colors,
  extendedSpacing,
  gridRequiredUtils,
  gridContainerRequiredUtils,
  hoverColors: getHoverColors(),
  textColors: Object.keys(colors).map((c) => "u-text-" + c),
  bgColors: Object.keys(colors).map((c) => "u-bg-" + c),
  whitelistGrid: [...getGridWithBreakpoints(), ...gridContainerRequiredUtils],
};
