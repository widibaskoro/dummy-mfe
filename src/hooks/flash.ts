import { createContainer } from "unstated-next";
import { useState, useEffect } from "react";

export const useFlash = createContainer((initialState = {}) => {
  const [flashObject, setFlashObject] = useState(initialState);
  const [toBeDeleted, setToBeDeleted] = useState([]);
  const toBeDeletedLength = toBeDeleted.length;
  const flashObjectLength = Object.keys(flashObject).length;

  const handleCloseFlash = (key) => {
    let data = flashObject[key];
    data.visible = false;
    setToBeDeleted((prev) => [...prev, key]);
    setFlashObject((prev) => ({ ...prev, [key]: data }));
  };

  const handleShowFlash = (req) => {
    const dataObject = {
      [req.id || Math.random()]: {
        visible: true,
        message: req.message,
        icon: req.icon,
        color: req.color,
        withCloseButton: req.withCloseButton || false
      }
    };
    setFlashObject({ ...flashObject, ...dataObject });
  };

  const reset = () => {
    setFlashObject({});
    setToBeDeleted([]);
  };

  useEffect(() => {
    if (toBeDeletedLength > 0 && toBeDeletedLength === flashObjectLength) {
      if (!flashObject[toBeDeleted]?.withCloseButton) {
        setTimeout(() => {
          reset();
        }, 2000);
      }
    }
  }, [toBeDeleted]);

  useEffect(() => {
    return () => {
      reset();
    };
  }, []);

  return {
    flashObject,
    handleCloseFlash,
    handleShowFlash
  };
});
