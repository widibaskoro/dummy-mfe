const zIndex = {
  modalDialog: 1052,
  modal: 1051,
  portal: 1060,
  modalOverlay: 1050,
  actionBottom: 102,
  sidebar: 101,
  topbar: 100,
  fileInput: 15,
  dropdown: 10,
  dropdownOverlay: 9,
  tooltip: 7,
  paginationActive: 3,
  fieldActive: 3,
  buttonHover: 3,
  buttonActive: 2,
  pagination: 1,
  default: 1,
  below: -1
};

export default zIndex;
