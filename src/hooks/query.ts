import qs from "query-string";

export const useQuery = (path): {} => {
  const query = qs.parse(path?.split("?").pop());
  return query;
};
