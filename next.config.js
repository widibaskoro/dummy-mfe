/** @type {import('next').NextConfig} */

const NextFederationPlugin = require("@module-federation/nextjs-mf");

const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  webpack: (config, options) => {
    if (!options.isServer) {
      config.plugins.push(
        new NextFederationPlugin({
          name: "gap",
          filename: "static/chunks/remoteEntry.js",
          remotes: {
            bangkokok:
              "bangkokok@http://localhost:5005/_next/static/chunks/remoteEntry.js",
          },
          exposes: {},
        })
      );
    }
    return config;
  },
};

module.exports = nextConfig;
